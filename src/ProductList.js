import React from "react";

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TextField from '@material-ui/core/TextField';

import { Button } from '@material-ui/core';


const useStyles = ({
  table: {
    minWidth: 650,
  },
});

class ProductList extends React.Component {


  constructor(props) {
    super(props);
    this.state = {
      error: null,
      products: [],
      id: "",
      userId: "",
      title: "",
      body: ""
    };
  }
  componentDidMount() {
    const apiUrl = "https://jsonplaceholder.typicode.com/posts";

    fetch(apiUrl)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            products: result,
            id: result.id
          })
        },
        (error) => {
          this.setState({ error });
        }
      );
  }

  editProduct = product => {

    console.log(product);

    this.setState({
      id: product.id,
      userId: product.userId,
      title: product.title,
      body: product.body
    })

  }
  updateProduct = (state) => {
    console.log({ state })



    this.setState({
      products: this.state.products.map((value) => {
        if (value.id === state.id) {
          return state;
        }
        return value;
      })
    })



  }


  handleNameChange(event) {
    this.setState({ userId: event.target.value });
  }
  handleTitleChange(event) {
    this.setState({ title: event.target.value });
  }

  handleValueChange(event) {
    this.setState({ body: event.target.value });
  }
  render() {


    const { error } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else {
      return (
        <div>
          <h2>ProductList</h2>

          <div class="row">
            <div class="col-md-6">
              <form>
                <TextField id="standard-basic" label="User Id" value={this.state.userId} onChange={this.handleNameChange.bind(this)} /> &nbsp;&nbsp;
                &nbsp;
                  <TextField
                  id="standard-multiline-static"
                  label="Title"
                  multiline
                  rows="4"
                  defaultValue="Default Value"

                  value={this.state.title} onChange={this.handleTitleChange.bind(this)}
                />&nbsp;&nbsp;&nbsp;


                <TextField
                  id="standard-multiline-static"
                  label="Body"
                  multiline
                  rows="4"
                  defaultValue="Default Value"

                  value={this.state.body} onChange={this.handleValueChange.bind(this)}
                />
                <br /><br />

                <Button variant="contained" onClick={this.updateProduct.bind(this, this.state)}>Update</Button>
              </form>

            </div>
          </div>

          {/* <Table>
            <thead>
              <tr>
                <th>#ID</th>
                <th>User Id</th>
                <th>Title</th>
                <th>Body</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {this.state.products.map(product => (
                <tr key={product.id}>
                  <td>{product.id}</td>
                  <td>{product.userId}</td>
                  <td>{product.title}</td>
                  <td>{product.body}</td>
                  <td><Button variant="info" onClick={() => this.editProduct(product)} >Edit</Button>
                  </td>
                </tr>
              ))}
            </tbody>

          </Table> */}
          <TableContainer >
            <Table className={useStyles.table} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>#ID</TableCell>
                  <TableCell align="right">User ID</TableCell>
                  <TableCell align="right">Title</TableCell>
                  <TableCell >Body</TableCell>
                  <TableCell align="right">Action</TableCell>

                </TableRow>
              </TableHead>
              <TableBody>
                {this.state.products.map(product => (
                  <TableRow key={product.id}>
                    <TableCell component="th" scope="row">
                      {product.id}
                    </TableCell>
                    <TableCell >{product.userId}</TableCell>
                    <TableCell >{product.title}</TableCell>
                    <TableCell >{product.body}</TableCell>
                    <TableCell ><Button variant="contained" onClick={() => this.editProduct(product)} >Edit</Button></TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </div>
      );
    }
  }
}

export default ProductList;
